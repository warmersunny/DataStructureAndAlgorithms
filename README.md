# DataStructureAndAlgorithms

### 介绍
Data structures and algorithms

### 软件架构
数据结构和算法练习项目

### 目标进度
+ 算法
 - [ ] 排序算法
 - [ ] 动态规划
 - [ ] DFS&BFS
+ 数据结构
 - [ ] 线性表
 - [ ] 栈
 - [ ] 队列
 - [ ] 哈希
 - [ ] 二叉树
 - [ ] 图
 
### 目录说明

1.  algorithms  -- 常见算法
2.  datastructure  -- 常用数据结构

#### 参考资料

1.  数据结构与算法分析


