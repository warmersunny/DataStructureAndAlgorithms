package com.warmer.algorithms.stack;

import java.util.*;

/**
 * @ClassName com.warmer.algorithms.stack.LeetCode946
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/13 20:14
 * @Version 1.0
 **/
public class LeetCode946 {
    
    public static boolean validateStackSequences(int[] pushed, int[] popped) {
        Stack<Integer> stack = new Stack<>();
        int j = 0;
        for (int i = 0; i < pushed.length; i++) {
            stack.push(pushed[i]);
            while (!stack.isEmpty() && stack.peek() == popped[j]) {
                j++;
                stack.pop();
            }
        }
        return stack.isEmpty();
    }
    
//    public static boolean validateStackSequences(int[] pushed, int[] popped) {
//        if(pushed.length <1) return true;
//        List<Integer> list = new ArrayList<>();
//        for (int i = 0; i < popped.length; i++) {
//            list.add(popped[i]);
//        }
//        Queue<Integer> queuePoped = new LinkedList<>(list);
//        Stack<Integer> stack = new Stack<>();
//        int idx = 0;
//        stack.push(pushed[idx]);
//        while (!queuePoped.isEmpty()) {
//            int top = stack.peek();
//            if (top == queuePoped.peek()) {
//                queuePoped.poll();
//                stack.pop();
//            } else {
//                idx++;
//                if(idx < pushed.length)
//                    stack.push(pushed[idx]);
//                else
//                    break;
//            }
//
//        }
//
//        return stack.isEmpty();
//    }
    
    /**
     *
     [1,2,3,4,5]
     [4,5,3,2,1]
     *
     * @param args
     */
    public static void main(String[] args) {
        int[] pushed = {1,2,3,4,5};
        int[] popped = {4,5,3,2,1};
        System.out.println(validateStackSequences(pushed, popped));
    }
}
