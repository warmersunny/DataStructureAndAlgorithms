package com.warmer.algorithms.stack;


import java.util.Stack;

/**
 * 两个栈实现队列
 */
public class QueueByStack {

    Stack<Integer> container = new Stack<>();
    Stack<Integer> temp = new Stack<>();


    /**
     * 入队
     * @return
     */
    public void offer(int ele){
        container.push(ele);
    }

    /**
     * 出队
     */
    public int poll() throws Exception {
        if(container.isEmpty()){
            throw new Exception("empty queue");
        }

        while(!container.isEmpty()){
            temp.push(container.pop());
        }

        int res = temp.pop();

        while(!temp.isEmpty()){
            container.push(temp.pop());
        }

        return res;
    }

    public boolean isEmpty(){
        return container.isEmpty();
    }

    public int peek(){
        while(!container.isEmpty()){
            temp.push(container.pop());
        }

        int peek = temp.peek();

        while(!temp.isEmpty()){
            container.push(temp.pop());
        }
        return peek;
    }


    public static void main(String[] args) throws Exception {

        QueueByStack queue = new QueueByStack();
        System.out.println(queue.isEmpty());
        queue.offer(1);
        queue.offer(2);

        System.out.println(queue.peek());

        System.out.println(queue.poll());
        System.out.println(queue.poll());
        queue.offer(3);
        System.out.println(queue.poll());
        System.out.println(queue.poll());
    }

}
