package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.MergeSort
 * @Description 归并排序
 * @Author wangh
 * @Date 2021/2/17 12:59
 * @Version 1.0
 **/
public class MergeSort {
    
    public static void main(String[] args) {
        int[] intArr = {1, 5, 6, 3, 8, 4, 0};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
    }
    
    public static void sort(int[] arr) {
        sort(arr, 0, arr.length - 1);
    }
    
    /**
     * 递归排序
     *
     * @param arr
     * @param l
     * @param r
     */
    public static void sort(int[] arr, int l, int r) {
        if (l >= r) {
            return;
        }
        int mid = (l + r) / 2;
        sort(arr, l, mid);
        sort(arr, mid + 1, r);
        if (arr[mid] > arr[mid + 1]) {
            merge(arr, l, mid, r);
        }
        
    }
    
    /**
     * 合并有序数组
     *
     * @param arr 原数组
     * @param l   起始下标
     * @param mid 中间下标
     * @param r   结束下标
     */
    public static void merge(int[] arr, int l, int mid, int r) {
        int[] aux = Arrays.copyOfRange(arr, l, r + 1);
        int j = l, k = mid + 1;
        for (int i = l; i <= r; i++) {
            if (j > mid) {
                // 如果左边数组处理完，将右边数组剩余部分合并到目标数组
                arr[i] = aux[k - l];
                k++;
            } else if (k > r) {
                // 如果右边数组处理完，将左边数组剩余部分合并到目标数组
                arr[i] = aux[j - l];
                j++;
            } else if (aux[j - l] < aux[k - l]) {
                // 如果两个数据都没有处理完则比较连个数组元素的大小，小的合并到目标数组
                arr[i] = aux[j - l];
                j++;
            } else {
                // 如果两个数据都没有处理完则比较连个数组元素的大小，小的合并到目标数组
                arr[i] = aux[k - l];
                k++;
            }
        }
        
    }
}
