package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.HeapSort
 * @Description 通过构建大顶堆，每次提取最大元素进行排序
 * 关键要素：
 * @Author wangh
 * @Date 2021/2/18 21:42
 * @Version 1.0
 **/
public class HeapSort {
    
    public static void main(String[] args) {
        // 移位运算优先级比 + - 低
//        System.out.println(4 << 1 + 1);
//        System.out.println(1  >> 1 + 1);
//        int[] intArr = {38, 65, 97, 76, 13, 27, 49, 100, 99, 111};
        int[] intArr = {2, 6, 1, 4, 8, 9, 7, 3};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
    }
    
    public static void sort(int[] arr) {
        heapSort(arr);
    }
    
    public static void heapSort(int[] tree) {
        // 每次构建一个大顶堆， 并将最大元素放置在数组的末尾，类似于冒泡排序的过程
        for (int n = tree.length - 1; n >= 0; n--) {
            buildHeap(tree, n);
            // 交换堆顶和数组末尾元素
            SortUtil.swap(tree, 0, n);
        }
    }
    
    /**
     * 构建大顶堆
     * 堆是完全二叉树，可以用数组表示
     * [3 ,2 ,1 ]
     * 表示如下堆（特殊的完全二叉树，大顶堆或者小顶堆）
     *   3
     *  /\
     * 2 1
     *
     * @param tree - 待排序数组
     * @param n    - 这里为了不使用额外的空间进行存储排好序的结果， 通过n来控制构建堆的数组元素范围，将排好序的元素放在原数组的末尾
     */
    public static void buildHeap(int[] tree, int n) {
        // 求最后一个叶子结点的父节点下标 公式   parentIndex = (i-1)/2
        // parent = ((tree.length - 1) - 1) / 2
        for (int i = (n - 1) >> 1; i >= 0; i--) {
            heapifyNode(tree, n, i);
        }
    }
    
    /**
     * 构建分支的堆（父子三个节点）
     *
     * @param tree 待构建数组
     * @param i    待构建元素下标
     */
    public static void heapifyNode(int[] tree, int n, int i) {
        int left = (i << 1) + 1;
        int right = (i << 1) + 2;
        if (left > n) {
            return;
        }
        // 判断是否右子节点为空
        int max = i;
        if (right > n) {
            max = max(tree, max, left);
        } else {
            max = max(tree, max(tree, max, left), right);
        }
        if (max != i) {
            // 交换最大元素下标和i下标的元素
            SortUtil.swap(tree, i, max);
        }
        
    }
    
    /**
     * 取数组中两个元素相对较大的下标
     *
     * @param tree 数组
     * @param x    下标1
     * @param y    下标2
     * @return
     */
    public static int max(int[] tree, int x, int y) {
        return tree[x] > tree[y] ? x : y;
    }
    
}
