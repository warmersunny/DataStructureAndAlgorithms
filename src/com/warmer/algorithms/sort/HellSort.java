package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.HellSort
 * @Description 希尔排序是对插入排序的优化，主要针对插入排序比较极端的情况交换次数过多的问题进行优化处理
 * @Author wangh
 * @Date 2021/2/16 21:14
 * @Version 1.0
 **/
public class HellSort {
    public static void main(String[] args) {
        int[] intArr = {38, 65, 97, 76, 13, 27, 49, 100, 99, 111};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
        
    }
    
    public static void sort(int[] arr) {
        // 设置间隔
        int gap = arr.length / 2;
        while (gap >= 1) {
            // =============插入排序开始=============
            for (int i = gap; i < arr.length - gap; i++) {
                int key = arr[i];
                int j ;
                for(j=i; j >= gap && key < arr[j - gap];   j -= gap){
                    arr[j] = arr[j - gap];
                }
                arr[j] = key;
            }
            // =============插入排序结束=============
            gap /= 2;
        }
    }
}
