package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.BubbleSort
 * @Description 冒泡排序(升序)
 * 通过两层循序进行排序，外层循环控制交换的轮次，每一轮找出一个最大值并且移动到数组的末尾，
 * 内层循环控制比较过程，如果前面的元素大于后面的元素，则进行交换。
 * @Author wangh
 * @Date 2021/2/14 11:11
 * @Version 1.0
 **/
public class BubbleSort {
    
    public static void main(String[] args) {
        int[] intArr = {1, 5, 2, 3, 8, 6, 9};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
        int[] intArr2 = {1, 5, 2, 3, 8, 6, 9};
        sortOptimz(intArr2);
        Arrays.stream(intArr2).forEach(System.out::println);
    }
    
    public static void sort(int[] arr) {
        // 考虑特殊情况，数组中的元素个数小于2
        int i;
        for (i = 0; i < arr.length; i++) {
            // 注意这里的边界处理
            for (int j = 0; j < (arr.length - 1 - i); j++) {
                // swap to nums
                if (arr[j] > arr[j + 1]) {
                    // 交换元素的前后位置
                    SortUtil.swap(arr, j, j + 1);
                }
            }
        }
        System.out.println("1.交换次数 i=" + i);
    }
    
    /**
     * 个人的小优化思路，外层循环设置一个标志位默认为true， 内层循环如果发生交换，标志位设为false
     */
    public static void sortOptimz(int[] arr) {
        // 考虑特殊情况，数组中的元素个数小于2
        int i;
        for (i = 0; i < arr.length; i++) {
            // 注意这里的边界处理
            boolean isSorted = true;
            for (int j = 0; j < (arr.length - 1 - i); j++) {
                // swap to nums
                if (arr[j] > arr[j + 1]) {
                    isSorted = false;
                    // 交换元素的前后位置
                    SortUtil.swap(arr, j, j + 1);
                }
            }
            if (isSorted) {
                break;
            }
        }
        System.out.println("2.交换次数 i=" + i);
    }
    
}
