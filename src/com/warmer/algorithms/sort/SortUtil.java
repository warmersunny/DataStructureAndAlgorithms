package com.warmer.algorithms.sort;

/**
 * @ClassName com.warmer.algorithms.sort.SortUtil
 * @Description 排序算法工具类
 * @Author wangh
 * @Date 2021/2/18 22:59
 * @Version 1.0
 **/
public class SortUtil {
    
    /**
     * 通过下标交换目标数组中的两个元素
     *
     * @param x - 下标1
     * @param y - 下标2
     */
    public static void swap(int[] arr, int x, int y) {
        int tmp = arr[x];
        arr[x] = arr[y];
        arr[y] = tmp;
    }
    
}
