package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.InsertSort
 * @Description 假设数组的前半段是已经排好序的数据，然后一次取后面一个元素，将元素插入到已排序数组的合适位置，
 * 知道最后一个元素插入完成，则数组排序完成。
 * @Author wangh
 * @Date 2021/2/16 20:33
 * @Version 1.0
 **/
public class InsertSort {
    
    public static void main(String[] args) {
//        int[] intArr = {38,65,97,76,13,27,49,100,99,111};
        int[] intArr = {1, 5, 6, 3, 8, 4, 0};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
    }
    
    /**
     * 通过移动元素，直接插入排序，而不是通过交换排序
     * @param arr
     */
    public static void sort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];
            int j = i;
            while(j > 0 && key < arr[j -1]){
                // 元素依次后移
                arr[j] = arr[j -1];
                j--;
            }
            arr[j] = key;
        }
    }
    
    /**
     * 不推荐
     * 这种写法是基于交换来实现的，严格意义上将不能叫做插入排序，
     * 实际上应该像上面那样只需要比较，而不需要每次进行交换
     *
     * @param arr
     */
    public static void sort_bad(int[] arr) {
        // 注意这里i的取值范围是 [0, arr.length-1)
        for (int i = 1; i < arr.length - 1; i++) {
            // 内层循环向前一次比较找到合适好的位置插入到已排好序的数组
            for (int j = i; j >= 1; j--) {
                if (arr[j] < arr[j - 1]) {
                    SortUtil.swap(arr, j, j - 1);
                }
            }
        }
    }
}
