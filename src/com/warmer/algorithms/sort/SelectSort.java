package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.SelectSort
 * @Description 选择排序
 * 首先找到数组中最小的元素，然后将它和数组中第一个元素交换位置（如果第一个元素就是最小元素，那么它就和自己交换）；
 * 其次，在剩余元素中找到最小的元素，将它和数组中第二个元素进行交换，以此类推，知道将整个数组排序。
 * @Author wangh
 * @Date 2021/2/16 20:00
 * @Version 1.0
 **/
public class SelectSort {
    public static void main(String[] args) {
        int[] intArr = {1, 5, 6, 3, 8, 4, 0};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
    }
    
    public static void sort(int[] arr) {
        // 外层循环确定要选择的位置，从数组头开始一
        for (int i = 0; i < arr.length - 1; i++) {
            // 内层循环负责后面元素和要比较的元素进行对比，判断是否需要交换
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[i]) {
                    // swap arr[j] and arr[i]
                    SortUtil.swap(arr, i, j);
                }
            }
        }
    }
}
