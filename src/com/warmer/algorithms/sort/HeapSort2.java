package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * 堆排序
 */
public class HeapSort2 {

    public static void main(String[] args) {
        System.out.println("排序 2, 6, 1, 4, 8, 9, 7, 3");
        int[] arr = {2, 6, 1, 4, 8, 9, 7, 3};
        heapSort(arr);
        Arrays.stream(arr).forEach(System.out::print);
        System.out.println();

        System.out.println("排序 2, 6, 1, 4, 8, 9, 7");
        int[] arr2 = {2, 6, 1, 4, 8, 9, 7};
        heapSort(arr2);
        Arrays.stream(arr2).forEach(System.out::print);
        System.out.println();

        System.out.println("排序空数组");
        int[] arr3 = {};
        heapSort(arr3);
        Arrays.stream(arr3).forEach(System.out::print);
    }

    public static void heapSort(int[] arr){
        for (int n = arr.length - 1; n >= 0; n--) {
            buildHeap(arr, n);
            // 交换堆顶和数组末尾元素
            SortUtil.swap(arr, 0, n);
        }
    }

    public static void buildHeap(int[] arr, int last) {
        for (int n = (arr.length - 1) / 2; n >= 0; n--) {
            heapify(arr, n, last);
        }
    }


    /**
     * @param arr    - 待排序数据
     * @param parent - 父节点索引
     */
    public static void heapify(int[] arr, int parent, int last) {
        int max = parent;
        int leftChild = (parent << 1) + 1;
        int rightChild = (parent << 1) + 2;

        if (rightChild <= last && arr[max] < arr[rightChild]) {
            max = rightChild;
        }

        if (leftChild <= last && arr[max] < arr[leftChild]) {
            max = leftChild;
        }

        if (max != parent) {
            swap(arr, max, parent);
        }

    }

    private static void swap(int[] arr, int idx1, int idx2) {
        int temp = arr[idx1];
        arr[idx1] = arr[idx2];
        arr[idx2] = temp;
    }
}

