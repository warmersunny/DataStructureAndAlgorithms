package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.MergeTwoSortedArray
 * @Description 归并排序的基础，合并连个有序数组
 * @Author wangh
 * @Date 2021/2/17 13:02
 * @Version 1.0
 **/
public class MergeTwoSortedArray {
    
    public static void main(String[] args) {
        int[] arr1 = {1, 3, 5, 7, 9};
        int[] arr2 = {2, 4, 6, 8, 10};
        int[] res = merge(arr1, arr2);
        Arrays.stream(res).forEach(System.out::println);
    }
    
    public static int[] merge(int[] arr1, int[] arr2) {
        int[] arr = new int[arr1.length + arr2.length];
        int j = 0, k = 0;
        for (int i = 0; i < arr.length; i++) {
            if (j >= arr1.length) {
                // 如果第一个数组处理完，将第二个数组剩余部分合并到目标数组
                arr[i] = arr2[k];
                k++;
            } else if (k >= arr2.length) {
                // 如果第二个数组处理完，将第一个数组剩余部分合并到目标数组
                arr[i] = arr1[j];
                k++;
            } else if (arr1[j] <= arr2[k]) {
                // 如果两个数据都没有处理完则比较连个数组元素的大小，小的合并到目标数组
                arr[i] = arr1[j];
                j++;
            } else {
                // 如果两个数据都没有处理完则比较连个数组元素的大小，小的合并到目标数组
                arr[i] = arr2[k];
                k++;
            }
        }
        
        return arr;
    }
}
