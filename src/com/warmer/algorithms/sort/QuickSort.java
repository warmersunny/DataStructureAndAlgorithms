package com.warmer.algorithms.sort;

import java.util.Arrays;

/**
 * @ClassName com.warmer.algorithms.sort.QuickSort
 * @Description 快速排序
 * @Author wangh
 * @Date 2021/2/17 21:24
 * @Version 1.0
 **/
public class QuickSort {
    
    public static void main(String[] args) {
        int[] intArr = {1, 5, 6, 3, 8, 4, 0};
        sort(intArr);
        Arrays.stream(intArr).forEach(System.out::println);
    }
    
    public static void sort(int[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }
    
    public static void quickSort(int[] arr, int left, int right) {
        if (left >= right) {return;}
        
        int partitionIndex = partition(arr, left, right);
        quickSort(arr, left, partitionIndex - 1);
        quickSort(arr, partitionIndex + 1, right);
    }
    
    public static int partition(int[] arr, int left, int right) {
        // 设置基准点为left
        int pivot = left;
        // index 为基准点最后应该插入的位置
        int index = left + 1;
        for (int i = index; i <= right; i++) {
            if (arr[i] < arr[pivot]) {
                SortUtil.swap(arr, i, index);
                index++;
            }
        }
        // 将基准点元素移动到最后一个比它小的元素进行交换，这样就满足左边都小于基准点，右边都大于基准点
        SortUtil.swap(arr, pivot, index - 1);
        return index - 1;
    }
    
}
