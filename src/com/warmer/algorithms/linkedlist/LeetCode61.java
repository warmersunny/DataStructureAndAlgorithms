package com.warmer.algorithms.linkedlist;

/**
 * @ClassName com.warmer.algorithms.linkedlist.LeetCode61
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/27 19:50
 * @Version 1.0
 **/
public class LeetCode61 {
    public ListNode rotateRight(ListNode head, int k) {
        if(head == null || head.next == null) return head;
        
        ListNode node = head;
        ListNode lastNode;
        // 求链表长度，并标记最后一个节点
        int length = 1;
        while(node.next != null){
            node = node.next;
            length++;
        }
        lastNode = node;
        
        // 对K进行取模
        k = k%length;
        if(k==0) return head;
        // 找到新的头结点位置
        ListNode kNode = head, pre = head;
        for(int i = 0 ; i< length - k; i++){
            pre = kNode;
            kNode = kNode.next;
        }
        
        // 新的头结点和老头结点以及末尾节点指针处理，形成新的链表
        ListNode oldHead = head;
        head = kNode;
        lastNode.next = oldHead;
        pre.next = null;
        
        return head;
    }
    
    /**
     *  Definition for singly-linked list.
     */
   
    static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }
}
