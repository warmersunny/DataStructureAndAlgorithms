package com.warmer.algorithms.linkedlist;

import com.warmer.datastructure.list.single.SingleLinkedList;
import com.warmer.datastructure.list.single.SingleLinkedNode;

/**
 * @ClassName com.warmer.algorithms.linkedlist.ReverseLinkedList
 * @Description 链表反转
 * @Author wangh
 * @Date 2021/3/2 21:42
 * @Version 1.0
 **/
public class ReverseLinkedList {
    
    /**
     * 反转单链表
     */
    public static void reverse(SingleLinkedList<Integer> singleLinkedList) {
        
        SingleLinkedNode<Integer> head = singleLinkedList.getHead();
        // 需要辅助变量
        SingleLinkedNode<Integer> pre = null;
        SingleLinkedNode<Integer> cur =  head.next;
        SingleLinkedNode<Integer> next = null;
        
        while (cur != null){
            // 保存后继节点
            next = cur.next;   // 1 记录后继节点
            cur.next = pre;    // 2 当前节点后继指针指向前一个节点
            pre = cur;         // 3 先移动前驱节点指针
            cur = next;        // 4 再移动当前节点指针指向下一个节点
        }
        head.next = pre;
    }
    
    public static void main(String[] args) {
        SingleLinkedList<Integer> singleLinkedList = new SingleLinkedList<>();
        SingleLinkedNode<Integer> node4 = new SingleLinkedNode<>("node4", 4);
        SingleLinkedNode<Integer> node3 = new SingleLinkedNode<>("node3", 3);
        SingleLinkedNode<Integer> node2 = new SingleLinkedNode<>("node2", 2);
        SingleLinkedNode<Integer> node1 = new SingleLinkedNode<>("node1", 1);
        singleLinkedList.insertToHead(node4);
        singleLinkedList.insertToHead(node3);
        singleLinkedList.insertToHead(node2);
        singleLinkedList.insertToHead(node1);
        // 反转前
        System.out.println("========== 链表反转前：");
        singleLinkedList.iterateList();
        System.out.println("========== 链表反转后：");
        reverse(singleLinkedList);
        singleLinkedList.iterateList();
        System.out.println(((int) Math.pow(10, 2)));
    }
}
