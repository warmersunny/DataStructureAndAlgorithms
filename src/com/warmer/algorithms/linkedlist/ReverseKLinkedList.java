package com.warmer.algorithms.linkedlist;

/**
 * @ClassName com.warmer.algorithms.linkedlist.ReverseKLinkedList
 * @Description 反转k个链表
 * @Author wangh
 * @Date 2021/3/2 23:26
 * @Version 1.0
 **/
public class ReverseKLinkedList {
    public static ListNode reverseKGroup(ListNode head, int k) {
        if (k <= 1) return head;
        int count = 0;
        ListNode node = head;
        ListNode lastTail = null;
        ListNode lastHead = head;
        while (node != null) {
            count++;
            // 计数达到K个，处理链表反转
            if (count % k == 0) {
                ListNode nextTemp = node.next;
                node.next = null;
                ListNode newHead = reverseList(lastHead);   // 反转链表，返回新头结点
                if (lastTail != null) {
                    lastTail.next = newHead;               // 新的链表尾lastTail 指向下一个待反转链表的头
                }
                if (count == k) head = newHead;             // 第一个子链表的头结点作为返回值
                
                lastTail = lastHead;                        // 保存已反转链表的尾部
                lastHead.next = nextTemp;                   // 链接后面的链表
                lastHead = nextTemp;                        // 指向后续的子链表的头结点
                node = nextTemp;                            // node指针后移
                continue;                                   // 防止后面的指针移动跳过节点
            }
            node = node.next;
        }
        
        return head;
    }
    
    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode cur = head;
        while (cur != null) {
            ListNode next = cur.next;    // 先保存后继节点
            cur.next = prev;             // 后继节点指针指向前驱节点
            prev = cur;                  // 前驱结点指针后移
            cur = next;                  // 当前节点指针后移
        }
        return prev;
    }
    
    public static void main(String[] args) {
        ListNode node1 = new ListNode(1);
        ListNode node2 = new ListNode(2);
        ListNode node3 = new ListNode(3);
        ListNode node4 = new ListNode(4);
        ListNode node5 = new ListNode(5);
        node1.next = node2;
        node2.next = node3;
        node3.next = node4;
        node4.next = node5;
        ListNode listNode = reverseKGroup(node1, 2);
        while (listNode != null) {
            System.out.println(listNode.val);
            listNode = listNode.next;
        }
    }
    
}

class ListNode {
    int val;
    ListNode next;
    
    ListNode() {
    }
    
    ListNode(int val) {
        this.val = val;
    }
    
    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}