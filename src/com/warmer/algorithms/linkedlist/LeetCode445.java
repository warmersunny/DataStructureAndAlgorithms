package com.warmer.algorithms.linkedlist;

/**
 * @ClassName com.warmer.algorithms.linkedlist.LeetCode445
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/12 22:06
 * @Version 1.0
 **/
public class LeetCode445 {
    // 解法有数字溢出问题
    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        int l1Num = 0, l2Num = 0;
        // 计算两个数字
        while (l1 != null) {
            l1Num = l1Num * 10 + l1.val;
            l1 = l1.next;
        }
        while (l2 != null) {
            l2Num = l2Num * 10 + l2.val;
            l2 = l2.next;
        }
        // 求和
        int sum = l1Num + l2Num;
        ListNode res = null;
        ListNode nextNode = new ListNode(0);
        res = nextNode;
        // 统计和的位数
        int n = sum;
        int count = 0;
        while (n / 10 > 0) {
            count++;
            n /= 10;
        }
        // 从高位开始创建链表
        while (count > 0) {
            int num = sum / ((int) Math.pow(10, count));
            nextNode.next = new ListNode(num);
            nextNode = nextNode.next;
            sum = sum % ((int) Math.pow(10, count));
            count--;
        }
        nextNode.next = new ListNode(sum);
        return res.next;
    }
    
    public static void main(String[] args) {
        ListNode l1_1 = new ListNode(7);
        ListNode l1_2 = new ListNode(2);
        ListNode l1_3 = new ListNode(4);
        ListNode l1_4 = new ListNode(3);
        l1_1.next = l1_2;
        l1_2.next = l1_3;
        l1_3.next = l1_4;
        
        ListNode l2_1 = new ListNode(5);
        ListNode l2_2 = new ListNode(6);
        ListNode l2_3 = new ListNode(4);
        l2_1.next = l2_2;
        l2_2.next = l2_3;
        ListNode listNode = addTwoNumbers(l1_1, l2_1);
        System.out.println(listNode);
    }
    
    static class ListNode{
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
        }
    }
}
