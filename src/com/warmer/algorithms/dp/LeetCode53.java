package com.warmer.algorithms.dp;

/**
 * @ClassName com.warmer.algorithms.dp.LeetCode53
 * @Description 动态规划  求数组中和最大的连续子序列
 * https://leetcode-cn.com/problems/maximum-subarray/
 * @Author wangh
 * @Date 2021/3/14 12:54
 * @Version 1.0
 **/
public class LeetCode53 {
    
    /**
     * 动态规划解法
     * @return
     */
    public static int maxSubArray_2(int[] nums){
        // 定义动态规划数组dp， dp[i] 表示 nums[0 - i] 区间数组的和最大连续子序列的和
        int maxNum = nums[0],pre = 0;
        for(int x : nums){
            pre = Math.max(pre + x , x);
            maxNum = Math.max(maxNum,pre);
        }
        return maxNum;
    }
    
    /**
     * 暴力解法
     * @param nums
     * @return
     */
    public static int maxSubArray_1(int[] nums) {
        if(nums.length == 1) return nums[0];
        // 暴力解法, 超出时间限制
        int max = nums[0];
        for(int i=0; i<nums.length ; i++){
            for(int j = i; j < nums.length; j++){
                int sumPre = nums[i];
                for (int k = i +1; k <=j; k++){
                    sumPre += nums[k];
                }
                max = Math.max(max, sumPre);
            }
        }
        
        return max;
    }
    
    public static void main(String[] args) {
        int[] arr = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
        int maxSubArray_1 = maxSubArray_1(arr);
        System.out.println(maxSubArray_1);
        int maxSubArray_2 = maxSubArray_2(arr);
        System.out.println(maxSubArray_2);
    
    }
}
