package com.warmer.algorithms.queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 两个队列实现栈
 */
public class StackByQueue {

    Queue<Integer> container = new LinkedList<>();
    Queue<Integer> temp = new LinkedList<>();

    public void push(int ele) {
        container.offer(ele);
    }

    public int pop() {
        int res = 0;
        while (!container.isEmpty()) {
            Integer poll = container.poll();
            if (!container.isEmpty())
                temp.offer(poll);
            else
                res = poll;
        }

        while (!temp.isEmpty()) {
            container.offer(temp.poll());
        }
        return res;
    }

    public int peek() {
        while (!container.isEmpty()) {
            temp.offer(container.poll());
        }
        int peek = temp.peek();

        while (!temp.isEmpty()) {
            container.offer(temp.poll());
        }
        return peek;
    }

    public boolean isEmpty() {
        return container.isEmpty();
    }

    public static void main(String[] args) {
        StackByQueue stack = new StackByQueue();

        stack.push(1);
        stack.push(2);
        stack.push(3);

        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.push(4);
        System.out.println(stack.peek());

    }
}
