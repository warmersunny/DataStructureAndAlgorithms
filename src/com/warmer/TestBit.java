package com.warmer;

import java.util.BitSet;

/**
 * @ClassName com.warmer.TestBit
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/13 21:07
 * @Version 1.0
 **/
public class TestBit {
    public static void main(String[] args) {
        Integer[] data = new Integer[1000000];
      
       
        System.out.println(data[1]);
        
        // 位集合
        BitSet bitSet = new BitSet();
        bitSet.set(1);
        bitSet.clear(1);
        boolean b = bitSet.get(1);
        System.out.println(b);
        System.out.println(1 << 20);
        System.out.println(1 << 16);
        System.out.println(1 << 8);
        System.out.println(1 << 4);
        System.out.println(1 << 2);
        System.out.println(1 << 1);
        System.out.println(1 >> 1);
        int i = 0x12;
        System.out.println(i);
        int n_100 = 0144;
        System.out.println(n_100);
    }
}
