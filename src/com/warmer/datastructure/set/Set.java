package com.warmer.datastructure.set;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.TreeSet;

/**
 * @ClassName com.warmer.datastructure.Set
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/9 22:09
 * @Version 1.0
 **/
public class Set {
    public static void main(String[] args) {
        java.util.Set<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);
        set.add(3);
//        set.forEach(System.out::println);
        
        ArrayList<Integer> res = new ArrayList<>();
        res.add(1);
        res.add(2);
        res.add(3);
        Integer[] toArray = res.toArray(new Integer[0]);
        int[] resArr = new int[res.size()];
        for(int i=0 ; i< res.size(); i++){
            resArr[i] = toArray[i];
        }
        Arrays.sort(resArr);
    }
}
