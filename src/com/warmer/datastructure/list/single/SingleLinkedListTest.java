package com.warmer.datastructure.list.single;

/**
 * @ClassName com.warmer.datastructure.list.SingleLinkedListTest
 * @Description TODO
 * @Author wangh
 * @Date 2021/2/24 19:21
 * @Version 1.0
 **/
public class SingleLinkedListTest {
    
    public static void main(String[] args) {
        SingleLinkedList<Integer> linkedList = new SingleLinkedList<>();
        SingleLinkedNode<Integer> node1 = new SingleLinkedNode<>("1", 1);
        SingleLinkedNode<Integer> node2 = new SingleLinkedNode<>("2", 2);
        linkedList.insertToHead(node1);
        linkedList.insertToHead(node2);
        linkedList.iterateList();
        System.out.println(linkedList.getLength());
        
        // delete a node from head
        linkedList.deleteFromHead();
        linkedList.iterateList();
        
        // get element by name
        SingleLinkedNode<Integer> node3 = new SingleLinkedNode<>("3", 3);
        linkedList.insertToHead(node3);
        SingleLinkedNode<Integer> nodeByName1 = linkedList.getNodeByName("1");
        SingleLinkedNode<Integer> nodeByName3 = linkedList.getNodeByName("3");
        System.out.println(nodeByName1);
        System.out.println(nodeByName3);
    }
   
}
