package com.warmer.datastructure.list.single;

/**
 * @ClassName com.warmer.datastructure.list.LinkedList
 * @Description LinkedList demo.
 * @Author wangh
 * @Date 2021/2/14 21:01
 * @Version 1.0
 **/
public class SingleLinkedList<T> {
    
    /**
     * head node
     */
    SingleLinkedNode<T> head;
    
    /**
     * length without head
     */
    int length;
    
    public void init() {
        head = new SingleLinkedNode<T>("head", null);
        length = 0;
    }
    
    public SingleLinkedList() {
        this.init();
    }
    
    public SingleLinkedNode<T> getHead() {
        return this.head;
    }
    
    /**
     * insert a node to the head of node list
     */
    public void insertToHead(SingleLinkedNode<T> node) {
        SingleLinkedNode<T> first = head.next;
        head.next = node;
        node.next = first;
        length++;
    }
    
    /**
     * iterate linkedlist
     */
    public void iterateList() {
        SingleLinkedNode<T> node = head.next;
        while (node != null) {
            System.out.println(node);
            node = node.next;
        }
    }
    
    /**
     * delete node from head
     */
    public void deleteFromHead() {
        SingleLinkedNode<T> node = head.next;
        if (node == null) return;
        head.next = node.next;
    }
    
    /**
     * get a element by name
     */
    public SingleLinkedNode<T> getNodeByName(String name) {
        SingleLinkedNode<T> node = head.next;
        if (node == null || name == null) return null;
        while (node != null) {
            if (name.equals(node.name)) {
                return node;
            }
            node = node.next;
        }
        return null;
    }
    
    /**
     * get length of the LinkedList
     */
    public int getLength() {
        return length;
    }
}

