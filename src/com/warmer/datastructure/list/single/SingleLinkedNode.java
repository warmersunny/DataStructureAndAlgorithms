package com.warmer.datastructure.list.single;

/**
 * @ClassName com.warmer.datastructure.list.SingleLinkedNode
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/2 21:45
 * @Version 1.0
 **/
public class SingleLinkedNode<T> {
    public String name;
    public T data;
    public SingleLinkedNode<T> next;
    
    public SingleLinkedNode(String name, T data) {
        this.name = name;
        this.data = data;
    }
    
    @Override
    public String toString() {
        return "LinkedNode{" +
                "name='" + name + '\'' +
                ", data=" + data +
                '}';
    }
}