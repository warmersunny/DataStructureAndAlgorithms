package com.warmer.datastructure.string;

/**
 * @ClassName com.warmer.datastructure.String
 * @Description TODO
 * @Author wangh
 * @Date 2021/3/14 23:00
 * @Version 1.0
 **/
public class StringReverse {
    
    /**
     *
     * 先反转左半部分，再反转右半部分，然后再反转全部
     * @param str
     * @param mid
     * @return
     */
    public static String reverseByMid(String str, int mid){
        String res = "";
        int n = str.length();
        if (n<=1 || mid >= n || mid ==0) return str;
        char[] chars = str.toCharArray();
    
        reverseArr(chars, 0, mid);
        reverseArr(chars, mid +1, n-1);
        reverseArr(chars, 0 , n-1);
        res = new String(chars);
        return res;
    }
    
    /**
     * reverse arr
     */
    public static void reverseArr( char[] arr, int start, int end){
        if (start == end) return;
        for(;start < end; start ++, end--){
            // 交换 i 和 n-1-i 处的元素
            char tmp = arr[start];
            arr[start] = arr[end];
            arr[end] = tmp;
        }
    }
    
    public static void main(String[] args) {
        String hello = "abcdef";
        String reverseByMid = reverseByMid(hello, 3);// efabcd
        System.out.println(reverseByMid);
    }
}
