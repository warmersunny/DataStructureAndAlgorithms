package com.warmer.datastructure.tree.binarytree;

import java.util.LinkedList;
import java.util.Stack;

/**
 * @Description 排序二叉树
 * @Author wangh
 * @Date 2021/2/28 20:10
 * @Version 1.0
 **/
public class BinaryTree {
    
    private BinaryNode[] nodeList;
    /**
     * root node
     */
    BinaryNode root;
    
    /**
     * build a tree from a int array
     *
     * @param arr - 二叉树表示数组
     */
    public BinaryNode build(int[] arr) {
        nodeList = new BinaryNode[arr.length];
        for (int i = 0; i < arr.length; i++) {
            nodeList[i] = new BinaryNode(arr[i]);
            if ((i - 1) % 2 == 0) nodeList[(i - 1) / 2].left = nodeList[i];
            if ((i - 1) % 2 == 1) nodeList[(i - 1) / 2].right = nodeList[i];
        }
        
        return nodeList[0];
    }
    
    public static void main(String[] args) {
        int[] arr = {1, 2, 4, 5, 7, 3, 6};
        BinaryTree bt = new BinaryTree();
        BinaryNode root = bt.build(arr);
        System.out.println("=============先序遍历=================");
        bt.preOrderIt(root);
        System.out.println();
        bt.preOrder(root);
        System.out.println();
        System.out.println("=============中序遍历=================");
        bt.midOrder(root);
        System.out.println();
        bt.midOrderIt(root);
        System.out.println();
        System.out.println("=============后序遍历=================");
        bt.postOrder(root);
        System.out.println();
        bt.postOrderIt(root);
        System.out.println();
        System.out.println("=============层序遍历=================");
        bt.levelOrder(root);
        System.out.println();
    }
    
    /**
     * pre order - 递归
     */
    public void preOrder(BinaryNode root) {
        if (root != null) {
            System.out.print(root.value + " ");
            preOrder(root.left);
            preOrder(root.right);
        }
    }
    
    /**
     * pre order - 迭代
     */
    public void preOrderIt(BinaryNode root) {
        if (root == null ) return;
        
        Stack<BinaryNode> nodeStack = new Stack<>();
        nodeStack.push(root);
        BinaryNode node;
        while (!nodeStack.isEmpty()) {
            node = nodeStack.pop();
            System.out.print(node.value + " ");
            if (node.right != null) nodeStack.push(node.right);
            if (node.left != null) nodeStack.push(node.left);
        }
    }
    
    
    /**
     * middle order - 递归
     */
    public void midOrder(BinaryNode root) {
        if (root != null) {
            midOrder(root.left);
            System.out.print(root.value + " ");
            midOrder(root.right);
        }
    }
    
    /**
     * middle order - 迭代
     */
    public void midOrderIt(BinaryNode root) {
        if (root == null ) return;
        
        Stack<BinaryNode> nodeStack = new Stack<>();
        BinaryNode node = root;
        while(node != null){
            nodeStack.push(node);
            node = node.left;
        }
    
        while( !nodeStack.isEmpty()){
            BinaryNode temp = nodeStack.pop();
            System.out.print(temp.value + " ");
            if (temp.right != null){
                temp = temp.right;
                nodeStack.push(temp);
                while (temp.left != null){
                    temp = temp.left;
                    nodeStack.push(temp);
                }
            }
        }
    }
    
    /**
     * post order - 递归
     */
    public void postOrder(BinaryNode root) {
        if (root != null) {
            postOrder(root.left);
            postOrder(root.right);
            System.out.print(root.value + " ");
        }
    }
    
    /**
     * post order - 迭代
     */
    public void postOrderIt(BinaryNode root) {
        if (root == null ) return;
        
        Stack<BinaryNode> stack = new Stack<>();
        Stack<BinaryNode> output = new Stack<>();
        BinaryNode node = root;
        while (node != null || !stack.isEmpty()) {
            if (node != null) {
                stack.push(node);
                output.push(node);
                node = node.right;
            } else {
                node = stack.pop();
                node = node.left;
            }
        }
    
        while (output.size() > 0) {
            BinaryNode n = output.pop();
            System.out.print(n.value + " ");
        }
    }
    
    /**
     * level order - 层序遍历
     */
    public void levelOrder(BinaryNode root) {
        LinkedList<BinaryNode> list = new LinkedList<>();
        list.add(root);
        BinaryNode node = null;
        while(!list.isEmpty()){
            node = list.poll();
            System.out.print(node.value + " ");
            if (node.left != null) list.offer(node.left);
            if (node.right != null) list.offer(node.right);
        }
    }
}

class BinaryNode {
    int value;
    BinaryNode left;
    BinaryNode right;
    
    BinaryNode(int value) {
        this.value = value;
    }
    
}
