package com.warmer.datastructure.tree.binarytree;

import java.util.Arrays;

/**
 * @ClassName com.warmer.datastructure.tree.binarytree.BinarySearchTree
 * @Description 二叉查找树（二叉排序树）
 * @Author wangh
 * @Date 2021/3/4 18:59
 * @Version 1.0
 **/
public class BinarySearchTree {
    
    public BSTNode root;
    
    public static void main(String[] args) {
//        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] arr = {5, 8, 4, 3, 1, 6, 7, 2, 9};
        BinarySearchTree bst = new BinarySearchTree();
        bst.root = new BSTNode(arr[0]);
        Arrays.stream(arr).forEach(el -> bst.insert(el));
        bst.midIterator(bst.root);
        System.out.println();
        System.out.println(bst.search(2));
        System.out.println(bst.search(0));
//
        System.out.println("删除叶子结点");
        bst.delete(2);
        bst.midIterator(bst.root);

//        System.out.println("删除带有一个子树的结点");
//        bst.delete(1);
//        bst.midIterator(bst.root);

//        System.out.println("删除带有两个个子树的结点");
//        bst.delete(5);
//        bst.midIterator(bst.root);
//        System.out.println("删除带有两个个子树的结点");
//        bst.delete(8);
//        bst.midIterator(bst.root);
    }
    
    boolean insert(int insertVal) {
        BSTNode newNode = new BSTNode(insertVal);
        if (this.root == null) {
            this.root = newNode;
        }
        BSTNode node = root;
        BSTNode parent;
        while (node != null) {
            parent = node;         // 记录父结点，后续生成新子结点使用
            // 比较插入
            int nodeValue = node.value;
            if (insertVal < nodeValue) {
                node = node.left;
                if (node == null) parent.left = newNode;
            } else if (insertVal > nodeValue) {
                node = node.right;
                if (node == null) parent.right = newNode;
            } else {
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * 二叉树的删除
     * 算法描述：
     * 1 若被删除的是叶子结点，则直接删除
     * 2 若被删除的结点只有一颗子树，则让其子树成为父节点的子树，代替被删除节点
     * 3 若被删除的结点有两颗子树，则让二叉树的中序序列后继结点代替该结点，并删除直接后继结点
     *
     * @param deleteVal
     * @return
     */
    boolean delete(int deleteVal) {
        if (root == null) return false;
        if (!search(deleteVal)) return false;
        BSTNode node = root;
        BSTNode parent = null;
        while (node != null) {
            
            if (node.value > deleteVal) {
                parent = node;
                node = node.left;
            } else if (node.value < deleteVal) {
                parent = node;
                node = node.right;
            } else {  // (node.value == deleteVal)
                int childIndex = getChildIndex(parent, deleteVal);
                // 删除节点
                if (node.left == null && node.right == null)  // 第一种情况, 叶子结点
                {
                    updateChildNode(parent, childIndex, null);
                } else if (node.left != null && node.right == null)  // 第二种情况 ， 只有左子树
                    updateChildNode(parent, childIndex, node.left);
                else if (node.left == null && node.right != null)  // 第二种情况 ， 只有右子树
                    updateChildNode(parent, childIndex, node.right);
                else {                                             // 第三种情况， 左子树和右子树都有
                    // 查找后继节点
                    BSTNode rChild = node.right;
                    BSTNode successor = rChild;
                    BSTNode succParent = node;
                    while (successor.left != null) {
                        succParent = successor;
                        successor = successor.left;
                    }
                    updateChildNode(parent, childIndex, successor);
                    int successorIndex = getChildIndex(succParent, successor.value);
                    if (succParent != node)
                        updateChildNode(succParent, successorIndex, successor.right);
                    
                    successor.left = node.left;
                    if (successor != node.right) {
                        successor.right = node.right;
                    }
                }
                return true;
            }
        }
        return false;
    }
    
    private void updateChildNode(BSTNode parent, int childIndex, BSTNode target) {
        if (parent == null) {
            root = target;
            return;
        }
        if (childIndex == 0)
            parent.left = target;
        else
            parent.right = target;
    }
    
    /**
     * 判断结点是父结点的左子树还是右子树
     *
     * @param parent
     * @param delVal
     * @return index  0 - 左子树， 1 - 右子树
     */
    int getChildIndex(BSTNode parent, int delVal) {
        int index = -1;
        if (parent == null) return -1;
        if (parent.right != null && parent.right.value == delVal) {
            index = 1;
        }
        if (parent.left != null && parent.left.value == delVal) {
            index = 0;
        }
        return index;
    }
    
    boolean search(int searchVal) {
        if (this.root == null) {
            return false;
        }
        BSTNode node = this.root;
        while (node != null) {
            if (node.value > searchVal) node = node.left;           // 大于则查找左子树
            else if (node.value < searchVal) node = node.right;     // 小于则查找右子树
            else return true;                                       // 相等则返回
        }
        return false;
    }
    
    /**
     * 中序遍历
     */
    public void midIterator(BSTNode node) {
        if (node != null) {
            midIterator(node.left);
            System.out.print(node.value + " ");
            midIterator(node.right);
        }
    }
    
}

//二叉排序树结点
class BSTNode {
    int value;
    BSTNode left;
    BSTNode right;
    
    BSTNode(int value) {
        this.value = value;
    }
}