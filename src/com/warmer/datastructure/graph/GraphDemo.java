package com.warmer.datastructure.graph;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;

import java.util.*;

/**
 * @ClassName com.warmer.datastructure.graph.GraphDemo
 * @Description 无向图 BFS and DFS
 * @Author wangh
 * @Date 2021/3/8 20:03
 * @Version 1.0
 **/
public class GraphDemo {
    
    
    public static void main(String[] args) {
        Map<Character, LinkedList<Character>> graph = getGraph();
        System.out.println("=========== 广度优先搜索");
        bfsGraph(graph, 'S');
        System.out.println("=========== 深度优先搜索");
        dfsGraph(graph, 'S');
    }
    
    /**
     * 获取一个邻接表表示的图
     * @return graph  - 邻接表表示的图
     */
    public static Map<Character, LinkedList<Character>> getGraph(){
        Map<Character, LinkedList<Character>> graph = new HashMap<>();
        // s顶点的邻接表
        LinkedList<Character> list_s = new LinkedList<Character>();
        list_s.add('A');
        list_s.add('B');
        LinkedList<Character> list_a = new LinkedList<Character>();
        list_a.add('S');
        list_a.add('D');
        LinkedList<Character> list_b = new LinkedList<Character>();
        list_b.add('S');
        list_b.add('C');
        LinkedList<Character> list_c = new LinkedList<Character>();
        
        list_c.add('B');
        list_c.add('D');
        LinkedList<Character> list_d = new LinkedList<Character>();
        list_d.add('A');
        list_d.add('C');
        list_d.add('E');
    
        //构造图
        graph.put('S', list_s);
        graph.put('A', list_a);
        graph.put('B', list_b);
        graph.put('C', list_c);
        graph.put('D', list_d);
        graph.put('E', new LinkedList<Character>());
        
        return graph;
    }
    
    /**
     * 图的广度优先遍历
     * 借助队列实现
     */
    public static void bfsGraph(Map<Character, LinkedList<Character>>  graph ,
                                Character start ){
        // 缓冲队列
        Queue<Character> queue = new LinkedList<>();
        // 存储顶点的访问记录
        Set<Character> visited = new HashSet<>();
        visited.add(start);
        queue.add(start);

        while(!queue.isEmpty()){
            Character peek = queue.poll();
            System.out.println(peek);
            LinkedList<Character> relatedCharacters = graph.get(peek);
            if (relatedCharacters != null && relatedCharacters.size() > 0){
                relatedCharacters.forEach(ch-> {
                    if(!visited.contains(ch)){
                        queue.offer(ch);
                        visited.add(ch);
                    }
                });
            }
        }
        
    }
    
    /**
     * 图的深度优先遍历
     *  借助栈实现
     */
    public static void dfsGraph(Map<Character, LinkedList<Character>>  graph ,
                                Character start){
        Stack<Character> stack = new Stack<>();
        // 存储与订单距离的map
        // value  0 未访问  1 访问了
        HashMap<Character, Integer> visited = new HashMap<>();
    
        stack.push(start);
        visited.put(start, 0);
        while(!stack.isEmpty()){
            Character peek = stack.pop();
            if(!visited.containsKey(peek) || visited.get(peek) == 0) {
                System.out.println(peek);  // 访问顶点
                visited.put(peek, 1);
            }
            
            LinkedList<Character> relatedCharacters = graph.get(peek);
            if (relatedCharacters == null && relatedCharacters.size() == 0) stack.pop();
            Character first = null;
            for (Character next : relatedCharacters) {
                if (!visited.containsKey(next)) {    // 获取当前出栈节点未被访问的第一个节点
                    if (first != null) {             // 除第一个节点以外，其他未被访问的结点先入栈
                        stack.push(next);
                        visited.put(next, 0);
                    } else {
                        first = next;
                    }
                }
            }
            
            if (first != null){                      // 除第一个节点最后入栈
                stack.push(first);
            }
        }
    }
    
}
